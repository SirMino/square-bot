package utility

import (
	"fmt"
	"strings"
	"time"
)

const (
	dateFormat = "02/01/06"
	timeFormat = "15:04"
)

func GetMissingString(dateTime time.Time) string {

	diff := time.Until(dateTime)
	days := uint(diff.Hours() / 24)
	hours := uint(diff.Hours())
	minutes := uint(diff.Minutes())

	if minutes < 0 {
		return ""
	}

	if days > 0 {
		hours = hours - days*24
		minutes = minutes - days*1440
	}
	if hours > 0 {
		minutes = minutes - hours*60
	}

	builder := strings.Builder{}

	builder.WriteString(fmt.Sprintf("📅 %s", dateTime.Format(dateFormat)))
	if dateTime.Hour() != 0 {
		builder.WriteString(fmt.Sprintf("\n🕒 %s", dateTime.Format(timeFormat)))
		builder.WriteString(", mancano")
		if days > 0 {
			builder.WriteString(fmt.Sprintf(" %d giorni", days))
		}
		if dateTime.Hour() != 0 {
			if hours != 0 {
				builder.WriteString(fmt.Sprintf(" %d ore", hours))
			}
			if minutes != 0 {
				builder.WriteString(fmt.Sprintf(" %d minuti", minutes))
			}
		}
	}

	return builder.String()
}

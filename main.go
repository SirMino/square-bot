package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math/rand/v2"
	"os"
	"strings"
	"time"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers"
	"mi.no/square-bot/utility"
)

type SquareHeadBot struct {
	Token    string
	Games    []Game
	Commands []BotCommand
	Randoms  Randoms
}

type BotCommand struct {
	ID      int    `json:"id"`
	Trigger string `json:"trigger"`
	Reply   string `json:"reply"`
	Enabled bool   `json:"enabled"`
}

type CommandsJson struct {
	Commands CommandsArray `json:"commands"`
}

type CommandsArray struct {
	Command []BotCommand `json:"command"`
}

type Randoms struct {
	Mercato []struct {
		Text   string `json:"text"`
		Active bool   `json:"active"`
	} `json:"mercato"`
	// AltraCategoria []struct {
	// 	Text   string `json:"text"`
	// 	Active bool   `json:"active"`
	// } `json:"altra categoria"`
}

type Game struct {
	category string
	when     time.Time
	vs       string
	where    string
	tv       string
	series   string
}

const (
	basketballEmoji = "\U0001F3C0"
	homeEmoji       = "\U0001F3E0"
	planeEmoji      = "\U00002708"
	yellowSquare    = "\U0001f7e8"
	redSquare       = "\U0001f7e5"
	dateFormat      = "02/01/06"
	timeFormat      = "15:04"
)

var (
	squareHeadBot  SquareHeadBot
	botInitialized bool = false
	lastUpdate     time.Time
)

func main() {

	token, found := os.LookupEnv("TELEGRAM_BOT_TOKEN")
	if !found {
		fmt.Println("No token env variable available.")
		os.Exit(1)
	}

	//squareHeadBot.Commands = make(map[string]string)
	if err := loadCommands(); err != nil {
		fmt.Println("Error loading commands ", err)
		os.Exit(1)
	}
	if err := loadRandoms(); err != nil {
		fmt.Println("Error loading randoms ", err)
		os.Exit(1)
	}

	ticker := time.NewTicker(1 * time.Hour)
	defer ticker.Stop()
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				fmt.Println("Done!")
			case _ = <-ticker.C:
				if err := loadCommands(); err != nil {
					fmt.Println("Error loading commands ", err)
					os.Exit(1)
				}
				if err := loadRandoms(); err != nil {
					fmt.Println("Error loading randoms ", err)
					os.Exit(1)
				}
			}
		}
	}()

	squareHeadBot.Token = token

	bot, err := gotgbot.NewBot(token, nil)
	if err != nil {
		panic("error " + err.Error())
	}
	dispatcher := ext.NewDispatcher(&ext.DispatcherOpts{
		Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
			log.Println("an error occurred while handling update:", err.Error())
			return ext.DispatcherActionNoop
		},
		MaxRoutines: ext.DefaultMaxRoutines,
	})
	updater := ext.NewUpdater(dispatcher, nil)

	// commands
	//dispatcher.AddHandler(handlers.NewCommand("nextgame", nextGame))
	//dispatcher.AddHandler(handlers.NewCommand("games", showGames))
	dispatcher.AddHandler(handlers.NewCommand("proxpartita", nextGame))
	dispatcher.AddHandler(handlers.NewCommand("calendario", showGames))
	dispatcher.AddHandler(handlers.NewCommand("giallo", showYellowTicket))
	dispatcher.AddHandler(handlers.NewCommand("rosso", showRedTicket))
	dispatcher.AddHandler(handlers.NewCommand("test", test))
	dispatcher.AddHandler(handlers.NewCommand("mercato", mercato))
	//dispatcher.AddHandler(handlers.NewCommand("cere", showOpenDay))
	for _, c := range squareHeadBot.Commands {
		if c.Enabled {
			dispatcher.AddHandler(handlers.NewCommand(c.Trigger, showMessage))
		}
	}

	err = updater.StartPolling(bot, &ext.PollingOpts{
		DropPendingUpdates: true,
		GetUpdatesOpts: &gotgbot.GetUpdatesOpts{
			Timeout: 9,
			RequestOpts: &gotgbot.RequestOpts{
				Timeout: time.Second * 10,
			},
		},
	})
	if err != nil {
		panic("failed to start polling " + err.Error())
	}

	log.Println("bot started.")
	updater.Idle()
}

func checkData() error {
	if !botInitialized {
		if err := initBot(); err != nil {
			return err
		}
	} else {
		delta := time.Since(lastUpdate)
		if delta.Minutes() > 60.0 {
			if err := loadGames(); err != nil {
				return err
			}
		}
	}

	return nil
}

func loadGames() error {
	file, err := os.Open("data/games.csv")
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	squareHeadBot.Games = nil
	for scanner.Scan() {
		csvRow := scanner.Text()
		if strings.HasPrefix(csvRow, "tipo") {
			continue
		}
		fields := strings.Split(csvRow, ";")

		gameDate, err := time.ParseInLocation(time.DateTime, fields[1], time.Local)
		if err != nil {
			return fmt.Errorf("error parsing date: %w", err)
		}
		squareHeadBot.Games = append(squareHeadBot.Games, Game{
			category: fields[0],
			when:     gameDate,
			vs:       fields[2],
			where:    fields[3],
			tv:       fields[4],
			series:   fields[5],
		})
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("error scanning games file: %w", err)
	}

	bubbleSortGames()

	lastUpdate = time.Now()

	return nil
}

func loadRandoms() error {

	jsonFile, err := os.ReadFile("data/randoms.json")
	if err != nil {
		return err
	}

	if err := json.Unmarshal(jsonFile, &squareHeadBot.Randoms); err != nil {
		return err
	}

	return nil
}

func loadCommands() error {

	jsonFile, err := os.ReadFile("data/commands.json")
	if err != nil {
		return err
	}

	var commandJson CommandsJson
	if err := json.Unmarshal(jsonFile, &commandJson); err != nil {
		return err
	}

	for _, c := range commandJson.Commands.Command {
		squareHeadBot.Commands = append(squareHeadBot.Commands, c)
	}

	return nil
}

func bubbleSortGames() {

	for i := 0; i < len(squareHeadBot.Games)-1; i++ {
		for j := 0; j < len(squareHeadBot.Games)-i-1; j++ {
			if squareHeadBot.Games[j].when.After(squareHeadBot.Games[j+1].when) {
				tmp := squareHeadBot.Games[j]
				squareHeadBot.Games[j] = squareHeadBot.Games[j+1]
				squareHeadBot.Games[j+1] = tmp
			}
		}
	}
}

func initBot() error {

	if err := loadGames(); err != nil {
		return err
	}

	botInitialized = true

	return nil
}

func inlineReply(bot *gotgbot.Bot, ctx *ext.Context) error {

	_, err := ctx.InlineQuery.Answer(bot, []gotgbot.InlineQueryResult{gotgbot.InlineQueryResultArticle{
		Id:      "11",
		Title:   "Title",
		Url:     "www.duckduckgo.org",
		HideUrl: true,
		InputMessageContent: gotgbot.InputTextMessageContent{
			MessageText: "blablablabla",
		},
		Description: "description bla",
	}}, &gotgbot.AnswerInlineQueryOpts{
		IsPersonal: true,
	})
	if err != nil {
		return fmt.Errorf("filed to send %w", err)
	}

	return nil
}

func nextGame(bot *gotgbot.Bot, ctx *ext.Context) error {

	if err := checkData(); err != nil {
		return err
	}

	var nextGame Game
	for _, g := range squareHeadBot.Games {
		if g.when.After(time.Now()) {
			nextGame = g
			break
		}
	}

	missing := utility.GetMissingString(nextGame.when)

	var cat string
	if nextGame.series == "" {
		cat = nextGame.category
	} else {
		cat = fmt.Sprintf("%s %s", nextGame.category, nextGame.series)
	}
	builder := strings.Builder{}
	builder.WriteString("Prossima Partita\n")
	builder.WriteString(fmt.Sprintf("<i>VS</i> <b>%s</b>\n", nextGame.vs))
	builder.WriteString(fmt.Sprintf("📍 %s\n", nextGame.where))
	builder.WriteString(fmt.Sprintf("🏆 %s\n", cat))
	if nextGame.tv != "" {
		builder.WriteString(fmt.Sprintf("📺 %s\n", nextGame.tv))
	}
	builder.WriteString(missing)

	return sendReply(bot, ctx, builder.String())
}

func showGames(bot *gotgbot.Bot, ctx *ext.Context) error {

	if err := checkData(); err != nil {
		return err
	}

	reply := "Ecco le prossime 8 partite:\n"
	gameCount := 0
	for _, g := range squareHeadBot.Games {
		if g.when.After(time.Now()) {
			if gameCount >= 8 {
				break
			}
			var cat string
			if g.series == "" {
				cat = g.category
			} else {
				cat = fmt.Sprintf("%s %s", g.category, g.series)
			}
			if g.when.Hour() == 0 {
				reply = reply + fmt.Sprintf(
					"%s [%s] %s <i>VS</i> <b>%s</b> presso %s\n",
					getGameIcon(g.where),
					cat,
					g.when.Format(dateFormat),
					g.vs,
					g.where)
			} else {
				reply = reply + fmt.Sprintf(
					"%s [%s] %s - %s <i>VS</i> <b>%s</b> presso %s\n",
					getGameIcon(g.where),
					cat,
					g.when.Format(dateFormat),
					g.when.Format(timeFormat),
					g.vs,
					g.where)
			}
			gameCount++
		}

	}
	return sendReply(bot, ctx, reply)
}

func getGameIcon(location string) string {
	if strings.EqualFold("bigi", location) || strings.EqualFold("palabigi", location) {
		return homeEmoji
	} else {
		return planeEmoji
	}
}

func showMessage(bot *gotgbot.Bot, ctx *ext.Context) error {
	command := ctx.EffectiveMessage.Text
	commandcut, _ := strings.CutPrefix(command, "/")
	var reply string
	for _, c := range squareHeadBot.Commands {
		if strings.Compare(c.Trigger, commandcut) == 0 {
			reply = c.Reply
			return sendReply(bot, ctx, reply)
		}
	}

	return nil
}

func showOpenDay(bot *gotgbot.Bot, ctx *ext.Context) error {
	dateTime := time.Date(2023, 8, 24, 19, 0, 0, 0, time.Now().Local().Location())
	missing := utility.GetMissingString(dateTime)
	if strings.EqualFold(missing, "") {
		return nil
	}
	return sendMessage(bot, ctx, fmt.Sprintf("🚨 Open day presso circolo CERE %s [<a href=\"https://www.pallacanestroreggiana.it/news/tutte-le-news/dopo-ferragosto-il-raduno-biancorosso-giovedi-24-lopen-day-al-cere\">click me</a>]", missing))
}

func showYellowTicket(bot *gotgbot.Bot, ctx *ext.Context) error {
	usersString, _ := strings.CutPrefix(ctx.EffectiveMessage.Text, "/giallo")
	usersString = strings.Trim(usersString, " ")
	if usersString == "" {
		return sendReply(bot, ctx, fmt.Sprintf("Devi dirmi a chi dare il cartellino, @%s nadero!", ctx.EffectiveMessage.From.Username))
	} else {
		fmt.Println(bot.Username)
		if usersString == "@"+bot.Username {
			return sendReply(bot, ctx, "Ve nano, vola bas!")
		}
		return sendMessage(bot, ctx, fmt.Sprintf("%s Cartellino GIALLO per %s", yellowSquare, usersString))
	}
}

func showRedTicket(bot *gotgbot.Bot, ctx *ext.Context) error {
	usersString, _ := strings.CutPrefix(ctx.EffectiveMessage.Text, "/rosso")
	usersString = strings.Trim(usersString, " ")
	if usersString == "" {
		return sendReply(bot, ctx, fmt.Sprintf("Devi dirmi a chi dare il cartellino, @%s nadero!", ctx.EffectiveMessage.From.Username))
	} else {
		fmt.Println(bot.Username)
		if usersString == "@"+bot.Username {
			return sendReply(bot, ctx, "Ve nano, vola bas!")
		}
		return sendMessage(bot, ctx, fmt.Sprintf("%s Cartellino <b>ROSSO</b> per %s", redSquare, usersString))
	}
}

func mercato(bot *gotgbot.Bot, ctx *ext.Context) error {
	strs := squareHeadBot.Randoms.Mercato
	guard := 0
	for {
		if guard >= 1000 {
			return sendMessage(bot, ctx, "no content")
		}
		str := strs[rand.IntN(len(strs))]
		if str.Active {
			return sendMessage(bot, ctx, str.Text)
		}
		guard += 1
	}
}

func test(bot *gotgbot.Bot, ctx *ext.Context) error {
	return sendMessage(bot, ctx, "test message")
}

func sendReply(bot *gotgbot.Bot, ctx *ext.Context, reply string) error {

	_, err := ctx.EffectiveMessage.Reply(bot, reply,
		&gotgbot.SendMessageOpts{
			ParseMode: "html",
		})
	if err != nil {
		return fmt.Errorf("failed to reply message: %w", err)
	}

	return nil
}

func sendMessage(bot *gotgbot.Bot, ctx *ext.Context, reply string) error {

	_, err := ctx.EffectiveChat.SendMessage(bot, reply,
		&gotgbot.SendMessageOpts{
			ParseMode: "html",
		})
	if err != nil {
		return fmt.Errorf("failed to send message: %w", err)
	}

	return nil
}
